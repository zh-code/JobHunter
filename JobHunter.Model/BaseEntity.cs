﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobHunter.Model
{
    public abstract class BaseEntity
    {
        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }

        [Required]
        public string CreatedByWho { get; set; }

        [Required]
        public string LastModifiedByWho { get; set; }

        [ForeignKey("CreatedByWho")]
        public virtual UserEntity Creator { get; set; }

        [ForeignKey("LastModifiedByWho")]
        public virtual UserEntity LastModifiedUser { get; set; }
    }
}