﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobHunter.Model
{
    public class JobCategoryEntity : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public string CategoryId { get; set; }

        [Required, Column(Order = 2)]
        public string CategoryName { get; set; }

        [Column(Order = 3)]
        public string ParentCategoryId { get; set; }

        [ForeignKey("ParentCategoryId")]
        public virtual JobCategoryEntity ParentJobCategories { get; set; }

        [InverseProperty("ParentCategoryId")]
        public virtual ICollection<JobCategoryEntity> ChildJobCategories { get; set; }
    }
}