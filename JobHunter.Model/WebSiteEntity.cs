﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobHunter.Model
{
    public class WebsiteEntity : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public string WebsiteId { get; set; }

        [Required, Column(Order = 2)]
        public string WebsiteName { get; set; }

        [Required, Column(Order = 3)]
        public string WebsiteUrl { get; set; }

        [InverseProperty("WebsiteId")]
        public virtual ICollection<JobEntity> Jobs { get; set; }
    }
}