﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobHunter.Model
{
    public class JobEntity : BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public string JobId { get; set; }

        [Required, Column(Order = 2)]
        public string WebsiteId { get; set; }

        [Required, Column(Order = 3)]
        public string CategoryId { get; set; }

        [Required, Column(Order = 4)]
        public string Url { get; set; }

        [Required, Column(Order = 5)]
        public DateTime PublishDate { get; set; }

        [Required, Column(Order = 6)]
        public string Location { get; set; }

        [Required, Column(Order = 7)]
        public string Title { get; set; }

        [Required, Column(Order = 8)]
        public string Description { get; set; }

        public int AmountOfOpening { get; set; }

        [ForeignKey("WebsiteId")]
        public virtual WebsiteEntity Website { get; set; }

        [ForeignKey("CategoryId")]
        public virtual JobCategoryEntity JobCategory { get; set; }
    }
}