﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobHunter.Model
{
    public class SkillEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public string SkillId { get; set; }

        [Required, Column(Order = 2)]
        public string SkillName { get; set; }

        [InverseProperty("SkillId")]
        public virtual ICollection<JobSkillEntity> JobsJunction { get; set; }
    }
}