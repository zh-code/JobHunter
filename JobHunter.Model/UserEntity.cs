﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobHunter.Model
{
    public class UserEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), Column(Order = 1)]
        public string UserId { get; set; }

        [Required, Column(Order = 2)]
        public string Email { get; set; }

        [Required, Column(Order = 3)]
        public string Username { get; set; }

        [Required, Column(Order = 4)]
        public string Salt { get; set; }

        [Required, Column(Order = 5)]
        public string PasswordHash { get; set; }

        [Required, Column(Order = 6)]
        public bool Verified { get; set; }

        [Required, Column(Order = 7)]
        public bool Disabled { get; set; }

        [InverseProperty("CreatedByWho")]
        public virtual ICollection<WebsiteEntity> WebsitesEntered { get; set; }

        [InverseProperty("LastModifiedByWho")]
        public virtual ICollection<WebsiteEntity> WebsitesModifed { get; set; }

        [InverseProperty("CreatedByWho")]
        public virtual ICollection<JobEntity> JobsEntered { get; set; }

        [InverseProperty("LastModifiedByWho")]
        public virtual ICollection<JobEntity> JobsModifed { get; set; }

        [InverseProperty("CreatedByWho")]
        public virtual ICollection<SkillEntity> SkillsEntered { get; set; }

        [InverseProperty("LastModifiedByWho")]
        public virtual ICollection<SkillEntity> SkillsModifed { get; set; }

        [InverseProperty("CreatedByWho")]
        public virtual ICollection<JobCategoryEntity> JobCategoriresEntered { get; set; }

        [InverseProperty("LastModifiedByWho")]
        public virtual ICollection<JobCategoryEntity> JobCategoriresModifed { get; set; }

        [InverseProperty("CreatedByWho")]
        public virtual ICollection<JobSkillEntity> JobSkillsEntered { get; set; }

        [InverseProperty("LastModifiedByWho")]
        public virtual ICollection<JobSkillEntity> JobSkillsModifed { get; set; }
    }
}