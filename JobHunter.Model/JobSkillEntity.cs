﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JobHunter.Model
{
    public class JobSkillEntity : BaseEntity
    {
        [Key]
        public string JobId { get; set; }

        [Key]
        public string SkillId { get; set; }

        [ForeignKey("JobId")]
        public virtual JobEntity Job { get; set; }

        [ForeignKey("SkillId")]
        public virtual SkillEntity Skill { get; set; }
    }
}
