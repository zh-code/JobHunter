﻿using System;
using System.Threading.Tasks;

namespace JobHunter.BusinessEngine.LogEngine
{
    public interface ILogger<TEntity> where TEntity : class
    {
        Task Info(Exception ex, string message = "");
        Task Debug(Exception ex, string message = "");
        Task Error(Exception ex, string message = "");
        Task Fatal(Exception ex, string message = "");
    }
}
