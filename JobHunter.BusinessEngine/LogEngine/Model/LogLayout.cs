﻿using System;

namespace JobHunter.BusinessEngine.LogEngine.Model
{
    public class LogLayout
    {
        public DateTime Date { get; set; }
        public LogLevelEnum Level { get; set; }
        public string AppName { get; set; }
        public string LoggerName { get; set; }
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
    }
}