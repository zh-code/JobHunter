﻿namespace JobHunter.BusinessEngine.LogEngine.Model
{
    public enum LogLevelEnum
    {
        INFO,
        DEBUG,
        ERROR,
        FATAL
    }
}