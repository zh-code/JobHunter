﻿using System;

namespace JobHunter.BusinessEngine.LogEngine.Logger
{
    internal class ConsoleLoggerOption
    {
        public ConsoleColor backgroundColor { get; set; }
        public ConsoleColor foregroundColor { get; set; }
    }
}