﻿using JobHunter.BusinessEngine.LogEngine.Model;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace JobHunter.BusinessEngine.LogEngine.Logger
{
    public class ConsoleLogger<TEntity> : ILogger<ConsoleLogger<TEntity>> where TEntity : class
    {
        private string appName;

        private static readonly BlockingCollection<(ConsoleLoggerOption option, LogLayout log)> logCollection =
            new BlockingCollection<(ConsoleLoggerOption, LogLayout)>();

        public ConsoleLogger(string appName)
        {
            this.appName = appName;
            Task.Run(() =>
            {
                while (true)
                {
                    (ConsoleLoggerOption option, LogLayout log) = logCollection.Take();
                    if (log != null && option != null)
                    {
                        Console.ForegroundColor = option.foregroundColor;
                        Console.BackgroundColor = option.backgroundColor;
                        Console.WriteLine($"{log.AppName} {log.LoggerName} {log.Date} {log.Level} {log.Message}");
                        Console.WriteLine(log.ExceptionMessage);
                        Console.ResetColor();
                    }
                }
            });
        }

        public async Task Info(Exception ex, string message = "")
        {
            var log = new LogLayout
            {
                Date = DateTime.Now,
                AppName = this.appName,
                Level = LogLevelEnum.INFO,
                LoggerName = typeof(TEntity).Name,
                ExceptionMessage = ex.ToString(),
                Message = message
            };
            var option = new ConsoleLoggerOption
            {
                backgroundColor = ConsoleColor.Black,
                foregroundColor = ConsoleColor.Gray
            };
            logCollection.Add((option, log));
        }

        public async Task Debug(Exception ex, string message = "")
        {
            var log = new LogLayout
            {
                Date = DateTime.Now,
                AppName = this.appName,
                Level = LogLevelEnum.DEBUG,
                LoggerName = typeof(TEntity).Name,
                ExceptionMessage = ex.ToString(),
                Message = message
            };
            var option = new ConsoleLoggerOption
            {
                backgroundColor = ConsoleColor.Black,
                foregroundColor = ConsoleColor.White
            };
            logCollection.Add((option, log));
        }

        public async Task Error(Exception ex, string message = "")
        {
            var log = new LogLayout
            {
                Date = DateTime.Now,
                AppName = this.appName,
                Level = LogLevelEnum.ERROR,
                LoggerName = typeof(TEntity).Name,
                ExceptionMessage = ex.ToString(),
                Message = message
            };
            var option = new ConsoleLoggerOption
            {
                backgroundColor = ConsoleColor.Black,
                foregroundColor = ConsoleColor.Yellow
            };
            logCollection.Add((option, log));
        }

        public async Task Fatal(Exception ex, string message = "")
        {
            var log = new LogLayout
            {
                Date = DateTime.Now,
                AppName = this.appName,
                Level = LogLevelEnum.FATAL,
                LoggerName = typeof(TEntity).Name,
                ExceptionMessage = ex.ToString(),
                Message = message
            };
            var option = new ConsoleLoggerOption
            {
                backgroundColor = ConsoleColor.Black,
                foregroundColor = ConsoleColor.Red
            };
            logCollection.Add((option, log));
        }
    }
}