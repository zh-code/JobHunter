﻿using JobHunter.Data;
using JobHunter.Model;
using System;
using System.Threading.Tasks;
using System.Transactions;

namespace JobHunter.DAL
{

    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private JobContext context;
        private TransactionScope transaction;

        private IRepository<UserEntity> userRepository;
        private IRepository<WebsiteEntity> websiteRepository;
        private IRepository<JobEntity> jobRepository;
        private IRepository<JobCategoryEntity> jobCategoryRepository;
        private IRepository<SkillEntity> skillRepository;
        private IRepository<JobSkillEntity> jobSkillRepository;

        public IRepository<UserEntity> UserRepository
        {
            get
            {
                if (this.userRepository == null)
                {
                    this.userRepository = new GenericRepository<UserEntity>(context);
                }
                return this.userRepository;
            }
        }

        public IRepository<WebsiteEntity> WebsiteRepository
        {
            get
            {
                if (this.websiteRepository == null)
                {
                    this.websiteRepository = new GenericRepository<WebsiteEntity>(context);
                }
                return this.websiteRepository;
            }
        }

        public IRepository<JobEntity> JobRepository
        {
            get
            {
                if (this.jobRepository == null)
                {
                    this.jobRepository = new GenericRepository<JobEntity>(context);
                }
                return this.jobRepository;
            }
        }

        public IRepository<JobCategoryEntity> JobCategoryRepository
        {
            get
            {
                if (this.jobCategoryRepository == null)
                {
                    this.jobCategoryRepository = new GenericRepository<JobCategoryEntity>(context);
                }
                return this.jobCategoryRepository;
            }
        }

        public IRepository<SkillEntity> SkillRepository
        {
            get
            {
                if (this.skillRepository == null)
                {
                    this.skillRepository = new GenericRepository<SkillEntity>(context);
                }
                return this.skillRepository;
            }
        }

        public IRepository<JobSkillEntity> JobSkillRepository
        {
            get
            {
                if (this.jobSkillRepository == null)
                {
                    this.jobSkillRepository = new GenericRepository<JobSkillEntity>(context);
                }
                return this.jobSkillRepository;
            }
        }

        public UnitOfWork()
        {
            this.context = new JobContext();
            this.transaction = new TransactionScope();
        }
        
        public async Task Save()
        {
            await context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            this.transaction.Complete();
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}