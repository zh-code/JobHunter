﻿using JobHunter.Model;
using System;
using System.Threading.Tasks;

namespace JobHunter.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<UserEntity> UserRepository { get; }
        IRepository<WebsiteEntity> WebsiteRepository { get; }
        IRepository<JobEntity> JobRepository { get; }
        IRepository<JobCategoryEntity> JobCategoryRepository { get; }
        IRepository<SkillEntity> SkillRepository { get; }
        IRepository<JobSkillEntity> JobSkillRepository { get; }

        Task Save();
    }
}