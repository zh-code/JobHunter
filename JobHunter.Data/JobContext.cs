﻿using JobHunter.Model;
using Microsoft.EntityFrameworkCore;
using System;

namespace JobHunter.Data
{
    public class JobContext : DbContext
    {
        public JobContext() : base()
        {
        }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<WebsiteEntity> Websites { get; set; }
        public DbSet<JobEntity> Jobs { get; set; }
        public DbSet<JobCategoryEntity> JobCategories { get; set; }
        public DbSet<SkillEntity> Skills { get; set; }
        public DbSet<JobSkillEntity> JobSkills { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseNpgsql(PGConnectionString.Get());
        }
    }

    internal static class PGConnectionString
    {
        public static string ServerAddress = Environment.GetEnvironmentVariable("POSTGRES_SERVERADDRESS");
        public static string ServerDatabase = Environment.GetEnvironmentVariable("POSTGRES_DATABASE");
        public static string ServerPassword = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");
        public static string ServerPort = Environment.GetEnvironmentVariable("POSTGRES_SERVERPORT");
        public static string ServerUsername = Environment.GetEnvironmentVariable("POSTGRES_USERNAME");

        public static string Get()
        {
            return string.Format("Host={0};Port={1};Username={2};Password={3};Database={4}", ServerAddress, ServerPort,
                ServerUsername, ServerPassword, ServerDatabase);
        }
    }
}
